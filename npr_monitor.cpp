#include <iostream>
#include <mbmonitor/AbstractMonitor.cpp>
#include <mbmonitor/MonitorFactory.h>

class ProducerConsumerMonitor : public mbmonitor::AbstractMonitor {
	private:
		int x;
		mbmonitor::Conditional c1;
		mbmonitor::Conditional c2;

	public:
		template<class Archive>
		void serialize(Archive &archive) {
			archive(x);
		}

		ProducerConsumerMonitor() {
			x = 1;
		}

		void increment_and_print_x() {
			if (x > 5) wait(c1);
			x++;
			std::cout << "x = " << x << " on process " << getRank() << std::endl;
			if (x == 1) signal(c2);
		}

		void decrement_and_print_x() {
			if (x <= 0) wait(c2);
			x--;
			std::cout << "x = " << x << " on process " << getRank() << std::endl;
			if (x == 5) signal(c1);
		}
};
 
class PrintingMonitor : public mbmonitor::AbstractMonitor {
	public:
		template<class Archive>
		void serialize(Archive &archive) {
		}

		void print_information() {
			std::cout << "process " << getRank() << " on monitor " << getId() << std::endl;	
		}

};

int main(int argc, char** argv) {

	mbmonitor::AbstractMonitor::init();
	
	auto prod_cons_monitor = mbmonitor::MonitorFactory::create<ProducerConsumerMonitor>();
	auto printing_monitor = mbmonitor::MonitorFactory::create<PrintingMonitor>();

	if (mbmonitor::AbstractMonitor::getRank() == 0) {
		for (int i=0; i < 10; i++) {
			prod_cons_monitor->increment_and_print_x();
			printing_monitor->print_information();
		}
		std::cout << "finished loop on process 0" << std::endl;
	}  else {
		for (int i=0; i < 5; i++)
			prod_cons_monitor->decrement_and_print_x();	

		std::cout << "finished loop on another process " << std::endl;
	}


	mbmonitor::AbstractMonitor::finalize();

	return 0;
}
