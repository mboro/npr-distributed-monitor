#ifndef MBMONITOR_CONDITIONAL_H
#define MBMONITOR_CONDITIONAL_H
namespace mbmonitor
{
//Conditional monitor variable
//can be passed as argument to AbstractMonitor methods
//signal and wait
class Conditional {
	private:
		static int seq_no;
		int my_seq_no;
	public:
		Conditional();
 		int get_sequence_number();
};
}

#endif
