#ifndef MBMONITOR_CONDITIONAL_CPP
#define MBMONITOR_CONDITIONAL_CPP
#include "Conditional.h"
namespace mbmonitor
{

Conditional::Conditional() {
	my_seq_no = seq_no++;
}
int Conditional::get_sequence_number() {
	return my_seq_no;
}

int Conditional::seq_no = 0;
}
#endif
