#ifndef MBMONITOR_ABSTRACT_MONITOR_CPP
#define MBMONITOR_ABSTRACT_MONITOR_CPP
#include "AbstractMonitor.h"
#include "exec_around.h"
#include "Token.cpp"
#include <mpi.h>
#include <thread>
#include <vector>
#include <chrono>
#include <cereal/archives/binary.hpp>
namespace mbmonitor
{

void AbstractMonitor::wait(Conditional &cond) {
	token.wait(cond.get_sequence_number());
}

void AbstractMonitor::signal(Conditional &cond) {
	token.signal(cond.get_sequence_number());
}

AbstractMonitor::AbstractMonitor() : token(monitorIdCounter, getWorldSize(), getRank()) {
	monitorId = monitorIdCounter++;
	monitors.push_back(this);
}


int AbstractMonitor::getRank() {
	return rank;
}	
int AbstractMonitor::getWorldSize() {
	return world_size;
}
int AbstractMonitor::getId() {
	return monitorId;
}

Token *AbstractMonitor::getToken() {
	return &token;
}

void AbstractMonitor::background_comm() {
	//runs constantly in the background until finalize is called
	std::vector<int> get_token_intent;
	get_token_intent.resize(3);

	while(true) {
		MPI_Recv(&get_token_intent[0], 3, MPI_INT, MPI_ANY_SOURCE,
			 100, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// monitor / counter / process rank
		if (get_token_intent[0] == -100) break;

		while (monitors.size() < get_token_intent[0] + 1) {  //TODO remove active waiting (it's rare)
			std::chrono::milliseconds duration(10);
			std::this_thread::sleep_for(duration);
		}

		monitors[get_token_intent[0]]->getToken()
			->update_counter(get_token_intent[2], get_token_intent[1]);
	}
}

void AbstractMonitor::init() {
	MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	bg_thread = std::thread(background_comm);
}

void AbstractMonitor::finalize() {
	MPI_Barrier(MPI_COMM_WORLD);
	if (getRank() == 0) {
		//inform listening threads about termination
		std::vector<int> token_intent;
		token_intent.push_back(-100);
		token_intent.push_back(-100);
		token_intent.push_back(-100);

		for (int i = 0; i < getWorldSize(); i++) {
			MPI_Ssend(&token_intent[0], 3, MPI_INT, i, 100, MPI_COMM_WORLD);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	bg_thread.join();
	MPI_Finalize();
}
		

int AbstractMonitor::rank = -1;
int AbstractMonitor::world_size = -1;
int AbstractMonitor::monitorIdCounter = 0;
std::vector<AbstractMonitor*> AbstractMonitor::monitors;
std::thread AbstractMonitor::bg_thread;
}

#endif
