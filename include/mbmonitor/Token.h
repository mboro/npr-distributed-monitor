#ifndef MBMONITOR_TOKEN_H
#define MBMONITOR_TOKEN_H

#include <vector>
#include <mutex>
#include <condition_variable>
#include "ISerializer.h"
namespace mbmonitor
{
std::mutex counters_mutex;
std::condition_variable waiter;
class Token {
	private:
		//processes waiting on conditional variables
		std::vector<int> conditional_proc;

		//variables beeing waited upon
		std::vector<int> conditional_vars;

		//counter value of request for token for each process 
		//at the time of last token ownership
		std::vector<int> had_token_counters;

		//current request counter value for each process 
		std::vector<int> counters;

		//serialized data from monitor (will be sent along with the token)
		std::string data;

		//request counter value for this process
		int counter;

		//id of monitor which owns this token
		int associatedMonitorId;

		//number of this MPI process
		int process_rank;

		//number of all MPI processes
		int num_proc;

		//is token currently in this process
		bool present;

		//at the last time this process tried to release the token
		//where there no buyers
		bool failedToPass;

		bool waiting;

		//send token to another process (executed by main or bg thread)
		void send(int receiverRank);
		
		//send serialized data
		void send_data(int receiverRank);

		//receive token from another process (executed by main thread)
		void recv();

		//receive serialized data
		void receive_data();

		//used to serialize and deserialize data
		ISerializer *serializer;

	public:
		void setSerializer(ISerializer *sr);

		//associatedMonitor = id of monitor which owns this token
		//num_proc = number of all MPI processes
		//process_rank = number of this MPI process
		Token(int associatedMonitor, int num_proc, int process_rank);

		//update request counter of another process 
		//(executed from bg thread)
		void update_counter(int process, int counter_value);

		//blocks until token is present in this process
		void acquire();

		//blocks until somebody call signal on conditional variable
		void wait(int conditional_number);

		//releases the token to a process waiting on conditional variable
		void signal(int conditional_number);

		//"release" the token; 
		//if somebody requested token it is sent immediately;
		//processes with lower rank always have higher priority 
		//(possible starvation)
		//returns true if token was passed to another process
		bool pass();

		void set_data(std::string data);
};
}
#endif
