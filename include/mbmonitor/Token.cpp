#ifndef MBMONITOR_TOKEN_CPP
#define MBMONITOR_TOKEN_CPP

#include <mpi.h>
#include "Token.h"
#include <stdio.h>
#include <string.h>
#include <algorithm>

namespace mbmonitor
{
Token::Token(int associatedMonitor, int num_proc, int process_rank) {
	associatedMonitorId = associatedMonitor; 
	this->process_rank =  process_rank; 	 
	this->num_proc = num_proc; 		 

	//token begins life in process 0
	if (process_rank == 0) {
		present = true;
	} else {
		present = false;
	}

	had_token_counters.resize(num_proc);
	counters.resize(num_proc);

	counter = 0;
	failedToPass = false;
	waiting = false;
}

void Token::update_counter(int process, int counter_value) {
	counters_mutex.lock();


	if (counters[process] < counter_value) {
		counters[process] = counter_value;

		if (failedToPass == true) {
			counters_mutex.unlock();
			pass();
			return;
		}
	}	

	counters_mutex.unlock();
}

void Token::acquire() {
	//if the token is already in this process return immediately
	if (present == true) {
		return;
	}

	//else wait for somebody to send the token
	counter++;

	std::vector<int> token_intent;
	token_intent.push_back(associatedMonitorId);
	token_intent.push_back(counter);
	token_intent.push_back(process_rank);

	//send request for token to all processes
	for (int i = 0; i < num_proc; i++) {
		MPI_Ssend(&token_intent[0], 3, MPI_INT, i, 100, MPI_COMM_WORLD);
	}

	//wait for the token
	recv();
	had_token_counters[process_rank]++;
}

void Token::setSerializer(ISerializer *sr) {
	serializer = sr;
}

void Token::wait(int conditional_number) {
	waiting = true;
	//serialize data
	conditional_proc.push_back(process_rank);
	conditional_vars.push_back(conditional_number);
	if (pass() == true) {
		std::unique_lock<std::mutex> lk(counters_mutex);
		waiter.wait(lk); //wait until token is sent to another process
	}
	recv();
	waiting = false;
}


void Token::signal(int conditional_number) { 
	auto it = std::find(conditional_vars.begin(), conditional_vars.end(), 
			    conditional_number);

	if (it != conditional_vars.end()) { //it was found
		int index = it - conditional_vars.begin();
		conditional_vars.erase(it);
		int receiver = conditional_proc[index];
		conditional_proc.erase(conditional_proc.begin() + index);

		send(receiver);
		acquire();
	}	
}

bool Token::pass() {
	std::unique_lock<std::mutex> lk(counters_mutex);
	if (present == false) return true;
	for (int i=0; i < num_proc; i++) {
		if (i != process_rank && counters[i] > had_token_counters[i]) {
			send(i);
			if (failedToPass == true && waiting == true) {
				failedToPass = false;
				lk.unlock();
				waiter.notify_one();
			}
			return false;	
		}
	}
	failedToPass = true;
	return failedToPass;
}

void Token::set_data(std::string data) {
	this->data = data;
}

void Token::send(int receiverRank) {
	present = false;
	int numElems = had_token_counters.size();

	MPI_Ssend(&process_rank, 1, MPI_INT, receiverRank, associatedMonitorId + 200, MPI_COMM_WORLD);
	MPI_Ssend(&numElems, 1, MPI_INT, receiverRank, 0, MPI_COMM_WORLD);

	if (numElems > 0) {
		MPI_Ssend(&had_token_counters[0], had_token_counters.size(), 
			 MPI_INT, receiverRank, 1, MPI_COMM_WORLD);
	}
	numElems = conditional_proc.size();
	MPI_Ssend(&numElems, 1, MPI_INT, receiverRank, 2, MPI_COMM_WORLD);

	if (numElems > 0) {
		MPI_Ssend(&conditional_proc[0], conditional_proc.size(), 
			 MPI_INT, receiverRank, 3, MPI_COMM_WORLD);
	}
	numElems = conditional_vars.size();
	MPI_Ssend(&numElems, 1, MPI_INT, receiverRank, 4, MPI_COMM_WORLD);

	if (numElems > 0) {
		MPI_Ssend(&conditional_vars[0], conditional_vars.size(), 
			 MPI_INT, receiverRank, 5, MPI_COMM_WORLD);
	}

	send_data(receiverRank);
}

void Token::send_data(int receiverRank) {
//serialize the data
	serializer->serialize();
	int nelems = data.size();
	char *message;

	message = new char[data.size()];
	std::fill_n(message, data.size(), 0);

	strcpy(message, data.c_str());

	MPI_Ssend(&nelems, 1, MPI_INT, receiverRank, 2333, MPI_COMM_WORLD);
	MPI_Ssend(message, data.size(), MPI_CHAR, receiverRank, 3333, MPI_COMM_WORLD);
	
	delete[] message;
}

void Token::receive_data() {
	int nelems;
	//TODO code working with multiple monitors
	MPI_Recv(&nelems, 1, MPI_INT, MPI_ANY_SOURCE, 2333, 
		 MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	char *message;
	message = new char[nelems];
	std::fill_n(message, nelems, 0);

	MPI_Recv(message, nelems, MPI_CHAR, MPI_ANY_SOURCE, 
		 3333, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	std::string msg(message, nelems);
	delete[] message;
	
	serializer->deserialize(msg);
}

void Token::recv() {
	MPI_Status status;
	int numElems = 0;
	int sender = -1;
	MPI_Recv(&sender, 1, MPI_INT, MPI_ANY_SOURCE, associatedMonitorId + 200, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	MPI_Recv(&numElems, 1, MPI_INT, sender, 0, MPI_COMM_WORLD, 
		 &status);


	had_token_counters.resize(numElems);
	if (numElems > 0) {
		MPI_Recv(&had_token_counters[0], numElems, MPI_INT, 
			 sender, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	MPI_Recv(&numElems, 1, MPI_INT, sender, 2, MPI_COMM_WORLD, 
		 MPI_STATUS_IGNORE);

	conditional_proc.resize(numElems);
	if (numElems > 0) {
		MPI_Recv(&conditional_proc[0], numElems, MPI_INT, 
			 sender, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
	
	MPI_Recv(&numElems, 1, MPI_INT, sender, 4, MPI_COMM_WORLD, 
		 MPI_STATUS_IGNORE);

	conditional_vars.resize(numElems);
	if (numElems > 0) {
		MPI_Recv(&conditional_vars[0], numElems, MPI_INT, 
			 sender, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}


	present = true;
	receive_data();
}
}

#endif
