#ifndef MBMONITOR_ISERIALIZER_H
#define MBMONITOR_ISERIALIZER_H

namespace mbmonitor
{

//interface simplifying serialization work
class ISerializer {
	public:
		virtual void deserialize(std::string msg) = 0;
		virtual void serialize() = 0;
};
}

#endif
