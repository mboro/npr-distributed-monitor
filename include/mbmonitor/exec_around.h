#ifndef REDI_EXEC_AROUND_H
#define REDI_EXEC_AROUND_H
#include <memory>
#include "tidy_ptr.h"
#include "ISerializer.h"
#include "Serializer.h"
#include <cereal/archives/binary.hpp>

using namespace redi;
namespace mbmonitor
{

//class which wrapps calls to a class methods
//by overloading -> operator
//and using refined smart pointer
template<typename T>
    class exec_around : ISerializer
    {
      template<typename> struct ptr;
	Serializer<T> serializer;

    public:
	void serialize() { 
			t.getToken()->set_data(serializer.serialize());
	}
	void deserialize(std::string msg) {
			serializer.deserialize(msg);
	}
      template<typename... Args>
        exec_around( Args&&... args) 
		: t{std::forward<Args>(args)...}, serializer(&t) 
	{
		t.getToken()->setSerializer(this);
	}

      ptr<exec_around> operator->() {
		return ptr<exec_around>(this);
	}
      ptr<const exec_around> operator->() const {
		return ptr<const exec_around>(this);
	}

    private:
      template<typename EA>
        struct ptr
        {
          tidy_ptr<EA> ea;

          explicit ptr(EA* ea) noexcept : ea(ea) { 
			ea->t.getToken()->acquire();
		 }
          ~ptr() { if (ea) {
			ea->t.getToken()->pass();
			}
		 }

          ptr(const ptr&) = delete;
          ptr& operator=(const ptr&) = delete;

          ptr(ptr&&) = default;

          auto operator->() const noexcept -> decltype(&ea->t)
          { return &ea->t; }
        };

      T t;
    };
}
#endif
