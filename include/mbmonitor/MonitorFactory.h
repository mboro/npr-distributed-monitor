#ifndef MBMONITOR_MONITOR_FACTORY_H
#define MBMONITOR_MONITOR_FACTORY_H

#include "AbstractMonitor.cpp"

namespace mbmonitor {

//is used to instantiate user defined monitors
class MonitorFactory {
	public:
		template<typename T, typename... Args>
		    static exec_around<T>
		    create(Args&&... args)
		    {
		      return exec_around<T>( std::forward<Args>(args)... );
		}
};
}
#endif
