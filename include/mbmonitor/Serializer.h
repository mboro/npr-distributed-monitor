#ifndef MBMONITOR_SERIALIZER_H
#define MBMONITOR_SERIALIZER_H
#include "ISerializer.h"
#include <sstream>
#include <string.h>
#include <algorithm>
#include <cereal/archives/binary.hpp>

namespace mbmonitor
{
//turns object into string and back
template<typename T>
	class Serializer
	{
		private:
			T *t_instance;
		public:
		Serializer(T *t) {
			t_instance = t;
		}
		std::string serialize() {
		//instance->string
			std::ostringstream ss;
			{
				cereal::BinaryOutputArchive oarchive(ss);
				oarchive(*t_instance);
			}
			return ss.str();
		}

		void deserialize(std::string data) {
		//string->instance
			std::istringstream ss(data);
			{
				cereal::BinaryInputArchive iarchive(ss);
				iarchive(*t_instance);
			}
		}
	};
}
#endif
