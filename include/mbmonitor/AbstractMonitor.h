#ifndef MBMONITOR_ABSTRACT_MONITOR_H
#define MBMONITOR_ABSTRACT_MONITOR_H

#include "Token.h"
#include "exec_around.h"
#include "Conditional.cpp"
#include <thread>

namespace mbmonitor
{

class AbstractMonitor {
	private:
		static std::thread bg_thread;
		static int rank;
		static int world_size;
		static int monitorIdCounter; //number of monitors in the system
		static std::vector<AbstractMonitor*> monitors;
		static void background_comm();

		Token token;
		int monitorId;

	protected:
		//blocks until another process calls signal on the same variable
		void wait(Conditional &cond);

		//wakes another process which waits on this conditional variable
		//if there is no such process function returns immediately
		void signal(Conditional &cond);

	public:
		//initializes environment; must be called exactly once
		static void init();

		//finalizes the environment; must be called exactly once
		static void finalize();

		//return unique identifier of this monitor
		int getId();

		//returns process number of the node executing code
		static int getRank();

		//returns number of all processes
		static int getWorldSize();

		AbstractMonitor();
		Token *getToken();

};
}
#endif
