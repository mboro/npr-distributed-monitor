EXECS=npr_monitor
MPICC?=mpic++
INC=-Iinclude
PROGRAM=npr_monitor
PROC_NUM=3

all: ${EXECS}

npr_monitor: npr_monitor.cpp 
	${MPICC} -std=c++11 -pthread -Wl,--no-as-needed ${INC} -o npr_monitor npr_monitor.cpp

run: npr_monitor
	mpirun -n ${PROC_NUM} ./${PROGRAM}
clean:
	rm -f ${EXECS}
